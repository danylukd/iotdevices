package dd.iot.vendor.view;

import dd.iot.vendor.VendorApi;

/**
 * 
 * @author danylukd
 *
 */
public interface VendorView {

	public void requestPersist(VendorApi<?> vendorApi);

	public void rescheduleUpdate(VendorApi<?> vendorApi, int updateInSeconds);
}
