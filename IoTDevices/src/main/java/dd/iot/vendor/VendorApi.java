package dd.iot.vendor;

import java.util.List;
import java.util.concurrent.Future;

import com.fasterxml.jackson.databind.JsonNode;

import dd.iot.device.utils.Couple;
import dd.iot.model.Device;
import dd.iot.model.Device.State;
import dd.iot.transport.AuthAccessor;
import dd.iot.transport.AuthAccessor.CredentialType;
import dd.iot.transport.ErrorHandler;
import dd.iot.transport.ErrorHandler.ErrorResponse;
import dd.iot.transport.Session;

public interface VendorApi<T> {

	public String getServiceUrl();

	public String authenticate(Session session);

	public ErrorHandler getErrorHandler();

	public List<Device> getDevices(Session session);

	public Future<ErrorResponse> setDeviceState(Device device, State<?> state, String... params);

	public T loadInto(JsonNode data);

	public JsonNode saveOut();

	public boolean isDataOkay();

	public void applyAuthCredential(AuthAccessor accessor, CredentialType type);

	public Couple<Integer, Integer> getRefreshInterval();
}
