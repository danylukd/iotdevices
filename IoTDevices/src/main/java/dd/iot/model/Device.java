package dd.iot.model;

import java.util.Arrays;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.function.Predicate;

import dd.iot.transport.ErrorHandler.ErrorResponse;
import dd.iot.vendor.VendorApi;

/**
 * Represents an IoT Device.
 *
 * @author Darcy Danyluk
 */
public class Device {

	public interface DeviceType {
		public State<?> getState(Map<String, String> metadata);
	}

	/**
	 * States the device can have.
	 * 
	 * @author danylukd
	 *
	 */
	public interface State<T> {

		/**
		 * Returns the int of the state
		 * 
		 * @return
		 */
		public String getVendorState();

		/**
		 * Returns the message
		 * 
		 * @return
		 */
		public String getMessage();

		public static <T> T getState(T[] enums, T defaultState, Predicate<T> filter) {
			T deviceState = Arrays.asList(enums).stream().filter(filter).findFirst().orElse(defaultState);
			return deviceState;
		}
	}

	private String id;
	private String alias;
	private String model;
	private String appServerUrl;
	private UUID sessionId;
	private DeviceType deviceType;

	private Map<String, String> deviceProperties;

	/**
	 * Device constructor.
	 * 
	 * @param id
	 *            the ID for the device (mandatory for access/control)
	 * @param model
	 *            the device's model.
	 * @param alias
	 *            the given friendly name of the device.
	 * @param appServerUrl
	 *            the URL the device for access/control.
	 */
	public Device(String id, DeviceType deviceType, String model, String alias, String appServerUrl, UUID sessionId) {
		this.id = id;
		this.deviceType = deviceType;
		this.model = model;
		this.alias = alias;
		this.appServerUrl = appServerUrl;
		this.sessionId = sessionId;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}

	/**
	 * @return the appServerUrl
	 */
	public String getAppServerUrl() {
		return appServerUrl;
	}

	/**
	 * Sets the device's state.
	 * 
	 * @param state
	 *            the state to set.
	 * @return {@link ErrorResponse} for the state setting request.
	 */
	public Future<ErrorResponse> setState(VendorApi<?> vendor, State<?> state, String... params) {

		Future<ErrorResponse> returnResposne = vendor.setDeviceState(this, state, params);

		return returnResposne;
	}

	public State<?> getState() {
		State<?> stateValue = deviceType.getState(getStatusProperties());
		return stateValue;
	}

	@Override
	public String toString() {
		return getAlias();
	}

	/**
	 * @return the sessionId
	 */
	public UUID getSessionId() {
		return sessionId;
	}

	/**
	 * @return the deviceType
	 */
	public DeviceType getDeviceType() {
		return deviceType;
	}

	public static Device getUnknownDevice() {
		return new Device("", new Device.DeviceType() {

			@Override
			public State<Object> getState(Map<String, String> metadata) {
				return new State<Object>() {
					@Override
					public String getVendorState() {
						return null;
					}

					@Override
					public String getMessage() {
						return "Unknown";
					}
				};
			}
		}, "Unknown", "Unknown", "", UUID.randomUUID());
	}

	/**
	 * @return the deviceProperties
	 */
	private Map<String, String> getStatusProperties() {
		if (this.deviceProperties == null) {
			this.deviceProperties = new ConcurrentHashMap <String, String>();
		}
		return this.deviceProperties;
	}
	
	public String getStatusProperty(String key) {
		return getStatusProperties().get(key);
	}
	
	public void setProperties(Map<String, String> properties, boolean clear) {
		if (clear) {
			getStatusProperties().clear();
		}
		getStatusProperties().putAll(properties);
	}
	
	public void setProperty(String key, String property) {
		getStatusProperties().put(key, property);
	}

}