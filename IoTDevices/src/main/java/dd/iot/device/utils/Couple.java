package dd.iot.device.utils;

import java.util.Objects;

/**
 * An immuatable container for holding two values.
 * 
 * @author danylukd
 *
 * @param <A>
 * @param <B>
 */
public final class Couple<A, B> {
	private A valueOne;
	private B valueTwo;

	public Couple(A valueOne, B valueTwo) {
		this.valueOne = valueOne;
		this.valueTwo = valueTwo;
	}

	/**
	 * @return the valueOne
	 */
	public A getValueOne() {
		return valueOne;
	}

	/**
	 * @return the valueTwo
	 */
	public B getValueTwo() {
		return valueTwo;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.valueOne, this.valueTwo);
	}

	@Override
	public boolean equals(Object object) {
		if (object == this) {
			return true;
		} else if (!(object instanceof Couple)) {
			return false;
		}
		Couple<?, ?> couple = (Couple<?, ?>) object;
		return Objects.equals(valueOne, couple.valueOne) && Objects.equals(valueTwo, couple.valueTwo);
	}

	@Override
	public String toString() {
		return Utils.join(getClass().getSimpleName(), "@", Integer.toHexString(hashCode()), "{valueOne=", this.valueOne,
				",valueTwo=", this.valueTwo, "}");
	}
}
