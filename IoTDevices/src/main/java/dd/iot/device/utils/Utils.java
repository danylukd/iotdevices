
package dd.iot.device.utils;

import java.lang.reflect.Field;
import java.security.Key;
import java.util.Arrays;
import java.util.Base64;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Tooltip;
import javafx.util.Duration;

/**
 * @author danylukd
 *
 */
public final class Utils {

	/**
	 * Performs a {@linkplain String#valueOf(Object)} and returns an empty string if value is null.
	 * 
	 * @param value
	 *            the value to string
	 * @return
	 */
	public static String valueOfNonNull(Object value) {
		if (value == null) {
			return "";
		}
		return String.valueOf(value);
	}

	/**
	 * Joins the varargs into a single string value.
	 * 
	 * @param values
	 *            the values to join
	 * @return
	 */
	public static String join(Object... values) {
		return Arrays.asList(values).stream().filter(Objects::nonNull).map(value -> value.toString())
				.collect(Collectors.joining());
	}

	/**
	 * Encrypts a given JSON with the given key
	 * 
	 * @param toEncrypt
	 *            the JSON to encrypt
	 * @param key
	 *            the key
	 * @return
	 */
	public static String encrypt(JsonNode toEncrypt, String key) {
		String encrypted = "";
		try {
			Cipher cipher = Cipher.getInstance("AES");
			Key aesKey = new SecretKeySpec(getKeyPadded(key).getBytes(), "AES");
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);
			byte[] encryptedBytes = cipher.doFinal(toEncrypt.toString().getBytes());
			encrypted = new String(Base64.getEncoder().encode(encryptedBytes), "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return encrypted;
	}

	/**
	 * Decrypts the String into a JSON with the given key.
	 * 
	 * @param toDecrypt
	 *            the String to decrypt.
	 * @param key
	 *            the key.
	 * @return
	 */
	public static JsonNode decrypt(String toDecrypt, String key) {
		JsonNode decrypted;
		try {
			Cipher cipher = Cipher.getInstance("AES");
			Key aesKey = new SecretKeySpec(getKeyPadded(key).getBytes(), "AES");
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			byte[] decryptedBytes = cipher.doFinal(Base64.getDecoder().decode(toDecrypt.getBytes()));
			String decryptedString = new String(decryptedBytes, "UTF-8");
			decrypted = new ObjectMapper().readTree(decryptedString);
		} catch (Exception e) {
			e.printStackTrace();
			decrypted = new ObjectNode(JsonNodeFactory.instance);
		}
		return decrypted;
	}

	/**
	 * Returns the key with padding
	 * 
	 * @param key
	 *            the key
	 * @return
	 */
	private static String getKeyPadded(String key) {
		StringBuilder keyPadded = new StringBuilder(key);
		do {
			keyPadded.append("|").append(key);
		} while (keyPadded.length() < 16);
		keyPadded.setLength(16);
		return keyPadded.toString();
	}

	/**
	 * Returns the given the deca-Fahrenheit value as a string value in Celsius
	 * 
	 * @param decaFahrenheit
	 *            the deca-Fahrenheit value (i.e. Fahrenheit x100)
	 * @return
	 */
	public static String getInCelsius(String decaFahrenheit) {
		if (decaFahrenheit == null || decaFahrenheit.isEmpty()) {
			return "-";
		}
		float rawCel = (Integer.valueOf(decaFahrenheit) - 320) * 0.0555555555555556f;
		String celsius = String.format("%.1f", rawCel);
		return celsius;
	}

	/**
	 * Stackoverflow-26854301 hack for Java8 setting durations.
	 * 
	 * @param tooltip
	 * @param activation
	 * @param hide
	 * @param left
	 */
	public static void hackTooltipStartTiming(Tooltip tooltip, int activation, int hide, int left) {
		try {
			Field fieldBehavior = tooltip.getClass().getDeclaredField("BEHAVIOR");
			fieldBehavior.setAccessible(true);
			Object objBehavior = fieldBehavior.get(tooltip);

			if (activation > 0) {
				setTooltipTimerField(objBehavior, "activationTimer", activation);
			}
			if (hide > 0) {
				setTooltipTimerField(objBehavior, "hideTimer", hide);
			}
			if (left > 0) {
				setTooltipTimerField(objBehavior, "leftTimer", left);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sets tooltip timer field value.
	 * 
	 * @param objBehavior
	 * @param field
	 * @param value
	 * @throws Exception
	 */
	private static void setTooltipTimerField(Object objBehavior, String field, int value) throws Exception {

		Field fieldTimer = objBehavior.getClass().getDeclaredField(field);
		fieldTimer.setAccessible(true);
		Timeline objTimer = (Timeline) fieldTimer.get(objBehavior);

		objTimer.getKeyFrames().clear();
		objTimer.getKeyFrames().add(new KeyFrame(new Duration(value)));
	}
}
