package dd.iot.example.desktop;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.JsonNode;

import dd.iot.device.utils.Couple;
import dd.iot.device.utils.Utils;
import dd.iot.example.desktop.vendor.Ecobee;
import dd.iot.example.desktop.vendor.Ecobee.EcobeeMode;
import dd.iot.example.desktop.vendor.Ecobee.EcobeeState;
import dd.iot.example.desktop.vendor.Ecobee.EcobeeType;
import dd.iot.example.desktop.vendor.EnvironmentCanada;
import dd.iot.example.desktop.vendor.TpLink;
import dd.iot.example.desktop.vendor.TpLink.TpLinkState;
import dd.iot.example.desktop.vendor.ui.VendorPane;
import dd.iot.model.Device;
import dd.iot.transport.AuthAccessor;
import dd.iot.transport.AuthAccessor.CredentialType;
import dd.iot.transport.ErrorHandler.ErrorResponse;
import dd.iot.vendor.VendorApi;
import dd.iot.vendor.view.VendorView;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * Sample desktop application for IoT devices.
 * 
 * @author Darcy Danyluk
 *
 */
public class IoTDevices extends Application implements VendorView {

	private static final String PERSIST_FILE = "iotd-conf";

	/*
	 * Use the host's IP address as the key for encrypting our data file. Users of this API should salt this value to
	 * their needs.
	 */
	private static String HOST_IP_ADDRESS;
	static {
		String ip = "127.0.0.1";
		try {
			ip = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException exception) {
			exception.printStackTrace();
		}
		HOST_IP_ADDRESS = ip;
	}

	private ExecutorService executor = Executors.newSingleThreadExecutor();
	private Runnable persistTask; // for saving the vendor configurations (e.g. credentials, etc.)
	// these two globals handle the updates for each vendor
	private ScheduledExecutorService vendorUpdateExecutor = Executors.newScheduledThreadPool(3);
	private Map<VendorPane, ScheduledFuture<?>> vendorScehduldedUpdates = new HashMap<VendorPane, ScheduledFuture<?>>();

	@Override
	public void start(Stage stage) throws Exception {

		stage.getIcons().add(new Image(getClass().getResourceAsStream("iotd.png")));

		final EnvironmentCanada envCanada = new EnvironmentCanada(this);
		VendorPane envCanadaPane = new VendorPane(envCanada, envCanada);
		envCanadaPane.initSession(envCanada);
		envCanadaPane.loadDevicesFromSession();
		sechuleUpdate(envCanadaPane);

		Map<Class<VendorApi<?>>, JsonNode> persitenceDataByClass = loadLocalPersistence();

		ComboBox<Device> devicesComboBox = new ComboBox<Device>();
		final TpLink tpLink = new TpLink(this);
		VendorPane tpLinkPane = new VendorPane(tpLink, tpLink) {
			@Override
			public void updateDevicesAndStatus(ObservableList<Device> options) {
				devicesComboBox.setItems(options);
				if (!options.isEmpty()) {
					devicesComboBox.getSelectionModel().select(0);
				}
			}
		};
		tpLinkPane.setupFromData(persitenceDataByClass.get(TpLink.class));
		sechuleUpdate(tpLinkPane);

		final Ecobee ecobee = new Ecobee(this, vendorUpdateExecutor);
		Label tempSetLabel = new Label("--");

		ComboBox<EcobeeMode> hvacModeComboBox = new ComboBox<EcobeeMode>();
		List<EcobeeMode> list = new ArrayList<EcobeeMode>();
		Arrays.asList(EcobeeMode.values()).stream().filter(mode -> mode.isSettable()).forEach(mode -> list.add(mode));
		hvacModeComboBox.setItems(FXCollections.observableArrayList(list));
		hvacModeComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<EcobeeMode>() {
			public void changed(ObservableValue<? extends EcobeeMode> observableValue, final EcobeeMode previousValue,
					final EcobeeMode newValue) {
				if (previousValue != null) {
					Device device = ecobee.getDevices(null).stream().findFirst().orElse(Device.getUnknownDevice());
					Platform.runLater(() -> ecobee.setDeviceState(device, newValue));
				}
			}
		});
		VendorPane ecobeePane = new VendorPane(ecobee, ecobee) {
			@Override
			public void updateDeviceStatusLabels(Device device, Function<Device, String> message,
					Function<Device, String> extraMessage, BiConsumer<Device, Label> messageEffects) {
				super.updateDeviceStatusLabels(device, message, extraMessage, messageEffects);
				if (device.getDeviceType() == EcobeeType.THERMOSTAT) {
					String temperature = "cool".equals(device.getStatusProperty("hvacMode"))
							? device.getStatusProperty("desiredCool")
							: device.getStatusProperty("desiredHeat");
					tempSetLabel.setText(Utils.getInCelsius(temperature));
				}
			}

			@Override
			public void updateDevicesAndStatus(ObservableList<Device> options) {
				Device thermostat = options.stream().filter(device -> device.getDeviceType() == EcobeeType.THERMOSTAT)
						.findFirst().orElse(null);
				if (thermostat != null) {
					String hvacMode = thermostat.getStatusProperty("hvacMode");
					hvacModeComboBox.getSelectionModel().select(EcobeeMode.getEcobeeMode(hvacMode));
				}
			}
		};
		ecobeePane.setupFromData(persitenceDataByClass.get(Ecobee.class));
		sechuleUpdate(ecobeePane);

		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent event) {
				if (event.getEventType() == WindowEvent.WINDOW_CLOSE_REQUEST) {
					saveLocalPersistence(tpLink, ecobee);
					vendorUpdateExecutor.shutdownNow();
					executor.shutdown();
				}
			}
		});
		persistTask = new Runnable() {
			public void run() {
				saveLocalPersistence(tpLink, ecobee);
			}
		};

		// state handling buttons
		Button onButton = new Button(TpLinkState.ON.getMessage());
		onButton.setPrefSize(75, 50);
		onButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						Optional.ofNullable(devicesComboBox.getSelectionModel().getSelectedItem())
								.orElse(Device.getUnknownDevice()).setState(tpLink, TpLinkState.ON);
						tpLinkPane.refreshAllDeviceStatuses();
					}
				});
			}
		});

		Button offButton = new Button(TpLinkState.OFF.getMessage());
		offButton.setPrefSize(75, 50);
		offButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						Optional.ofNullable(devicesComboBox.getSelectionModel().getSelectedItem())
								.orElse(Device.getUnknownDevice()).setState(tpLink, TpLinkState.OFF);
						tpLinkPane.refreshAllDeviceStatuses();
					}
				});
			}
		});

		Button loginButton = new Button("Login");
		loginButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Platform.runLater(() -> showUserPasswordDialog(tpLink, tpLinkPane));
			}
		});

		Button authenticateButton = new Button("Authenticate");
		authenticateButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Platform.runLater(() -> showAuthenticateDialog(ecobee, ecobeePane));
			}
		});

		GridPane mainPane = new GridPane();
		mainPane.setPadding(new Insets(3, 3, 3, 3));
		int mainPaneRow = 0;
		mainPane.add(envCanadaPane, 0, mainPaneRow++);
		mainPane.add(new Separator(Orientation.HORIZONTAL), 0, mainPaneRow++);

		int row = 1; // first row (0) is device statuses
		tpLinkPane.add(devicesComboBox, 0, row++);
		tpLinkPane.add(new Separator(Orientation.VERTICAL), 0, row++);
		GridPane buttonPane = new GridPane();
		buttonPane.add(onButton, 0, 0);
		buttonPane.add(new Separator(Orientation.VERTICAL), 1, 0);
		buttonPane.add(offButton, 2, 0);
		tpLinkPane.add(buttonPane, 0, row++);
		tpLinkPane.add(new Separator(Orientation.VERTICAL), 0, row++);
		GridPane gridPaneButtons = new GridPane();
		tpLinkPane.add(gridPaneButtons, 0, row++);
		gridPaneButtons.add(new Separator(Orientation.VERTICAL), 1, 0);
		gridPaneButtons.add(loginButton, 2, 0);

		tpLinkPane.add(new Separator(Orientation.VERTICAL), 0, row);
		mainPane.add(tpLinkPane, 0, mainPaneRow++);

		GridPane ecobeeControlsPane = new GridPane();
		Button tempCancelButton = new Button("Resume");
		tempCancelButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Platform.runLater(() -> changeEcobeeState(ecobeePane, EcobeeState.TEMPERATURE_RESUME, tempSetLabel,
						thermostat -> "cool".equals(thermostat.getStatusProperty("hvacMode"))
								? thermostat.getStatusProperty("desiredCool")
								: thermostat.getStatusProperty("desiredHeat")));
			}
		});
		int col = 0;
		ecobeeControlsPane.add(tempCancelButton, col++, 0);
		Separator separator = new Separator(Orientation.VERTICAL);
		separator.setMinWidth(15);
		ecobeeControlsPane.add(separator, col++, 0);

		Button tempDownButton = new Button(EcobeeState.TEMPERATURE_DOWN.getMessage());
		tempDownButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Platform.runLater(() -> changeEcobeeState(ecobeePane, EcobeeState.TEMPERATURE_DOWN, tempSetLabel,
						thermostat -> thermostat.getStatusProperty("newTemp")));
			}
		});
		ecobeeControlsPane.add(tempDownButton, col++, 0);
		separator = new Separator(Orientation.VERTICAL);
		separator.setMinWidth(5);
		ecobeeControlsPane.add(separator, col++, 0);

		ecobeeControlsPane.add(tempSetLabel, col++, 0);
		separator = new Separator(Orientation.VERTICAL);
		separator.setMinWidth(5);
		ecobeeControlsPane.add(separator, col++, 0);

		Button tempUpButton = new Button(EcobeeState.TEMPERATURE_UP.getMessage());
		tempUpButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Platform.runLater(() -> changeEcobeeState(ecobeePane, EcobeeState.TEMPERATURE_UP, tempSetLabel,
						thermostat -> thermostat.getStatusProperty("newTemp")));
			}
		});
		ecobeeControlsPane.add(tempUpButton, col++, 0);
		ecobeeControlsPane.add(new Separator(Orientation.HORIZONTAL), 0, 1);

		row = 1; // first row (0) is device statuses
		ecobeePane.add(ecobeeControlsPane, 0, row++);

		GridPane ecobeeModeAuthPane = new GridPane();
		ecobeeModeAuthPane.add(hvacModeComboBox, 0, 0);
		separator = new Separator(Orientation.VERTICAL);
		separator.setMinWidth(5.0);
		ecobeeModeAuthPane.add(separator, 1, 0);
		ecobeeModeAuthPane.add(authenticateButton, 2, 0);

		ecobeePane.add(ecobeeModeAuthPane, 0, row++);

		mainPane.add(new Separator(Orientation.HORIZONTAL), 0, mainPaneRow++);
		mainPane.add(ecobeePane, 0, mainPaneRow);

		stage.setScene(new Scene(mainPane, 165, 290));
		stage.setX(3);
		stage.setY(3);
		stage.setTitle("IoT Devices");
		stage.show();
	}

	private void changeEcobeeState(VendorPane ecobeePane, EcobeeState newState, Label tempSetLabel,
			Function<Device, String> updateText) {
		final Font font = Font.getDefault();
		tempSetLabel.setFont(Font.font(font.getFamily(), FontPosture.ITALIC, font.getSize()));
		Device thermostat = ecobeePane.getDevices().stream()
				.filter(device -> device.getDeviceType() == EcobeeType.THERMOSTAT).findFirst().get();

		Future<ErrorResponse> results = ecobeePane.getVendorApi().setDeviceState(thermostat, newState);
		tempSetLabel.setText(Utils.getInCelsius(updateText.apply(thermostat)));

		executor.execute(() -> handleEcobeeStateResults(results, tempSetLabel));
	}

	private static void handleEcobeeStateResults(Future<ErrorResponse> results, Label tempSetLabel) {
		final Font font = Font.getDefault();
		ErrorResponse error = null;
		try {
			error = results.get();
		} catch (InterruptedException | ExecutionException | CancellationException e) {
			e.printStackTrace();
		}
		if (error == ErrorResponse.ERR_NONE) {
			tempSetLabel.setFont(Font.font(font.getFamily(), FontPosture.REGULAR, font.getSize()));
		}

	}

	private void saveLocalPersistence(VendorApi<?>... vendors) {
		try {
			Path appPath = getLocalPersistenceFile();
			String fileLines = Arrays.asList(vendors).stream()
					.map(vendor -> new StringBuilder(vendor.getClass().getName()).append("=")
							.append(Utils.encrypt(vendor.saveOut(), HOST_IP_ADDRESS).replace("=", "%3D")))
					.collect(Collectors.joining("\n"));
			Files.write(appPath, fileLines.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Path getLocalPersistenceFile() {
		Path appPath = null;

		String appDataPath = new StringBuffer(System.getProperty("user.home")).append(File.separator).append("AppData")
				.append(File.separator).append("Local").append(File.separator).append(getClass().getSimpleName())
				.append(File.separator).append(PERSIST_FILE).toString();
		try {
			appPath = Paths.get(appDataPath);
			if (!Files.exists(appPath.getParent())) {
				Files.createDirectories(appPath.getParent());
			}
			if (!Files.exists(appPath)) {
				Files.createFile(appPath);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return appPath;
	}

	@SuppressWarnings("unchecked")
	private Map<Class<VendorApi<?>>, JsonNode> loadLocalPersistence() {
		Map<Class<VendorApi<?>>, JsonNode> persitenceDataByClass = new HashMap<Class<VendorApi<?>>, JsonNode>();
		try {
			Path appPath = getLocalPersistenceFile();
			List<String> configurations = Files.readAllLines(appPath);
			for (String line : configurations) {
				String[] conf = line.split("=");
				try {
					persitenceDataByClass.put((Class<VendorApi<?>>) Class.forName(conf[0]),
							Utils.decrypt(conf[1].replace("%3D", "="), HOST_IP_ADDRESS));
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return persitenceDataByClass;
	}

	private void showAuthenticateDialog(Ecobee ecobee, VendorPane ecobeePane) {
		Dialog<String> dialog = new Dialog<String>();
		dialog.setTitle(new StringBuilder("Authentication: ").append(ecobee.getClass().getSimpleName()).toString());

		ButtonType authButtonType = new ButtonType("Authenticate", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(authButtonType, ButtonType.CANCEL);
		Node authButton = dialog.getDialogPane().lookupButton(authButtonType);
		authButton.setDisable(ecobee.getApiKey() == null || ecobee.getPin() == null);

		GridPane gridPane = new GridPane();
		gridPane.setPadding(new Insets(5, 5, 5, 5));
		gridPane.setHgap(3);
		gridPane.setVgap(3);

		int row = 0;

		Button pinRequestButton = new Button("Request PIN");

		TextField apiKeyTextField = new TextField();
		apiKeyTextField.setText(ecobee.getApiKey());
		gridPane.add(new Label("API key (Client ID):"), 0, row);
		gridPane.add(apiKeyTextField, 1, row++);
		apiKeyTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			boolean valueValid = newValue != null && !newValue.trim().isEmpty();
			pinRequestButton.setDisable(!valueValid);
			if (valueValid) {
				ecobee.setApiKey(newValue);
			}
		});

		TextField pinTextField = new TextField();
		pinRequestButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						if (confirmPinRequest()) {
							String pin = ecobee.requestPin();
							pinTextField.setText(pin);
							if (pin != null && !pin.trim().isEmpty()) {
								authButton.setDisable(false);
							}
						}
					}
				});
			}
		});

		pinTextField.setEditable(false);
		gridPane.add(new Label("PIN:"), 0, row);
		gridPane.add(pinTextField, 1, row);
		pinTextField.setText(ecobee.getPin());
		pinRequestButton.setDisable(ecobee.getApiKey() == null || ecobee.getApiKey().trim().isEmpty());

		gridPane.add(pinRequestButton, 2, row++);

		apiKeyTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			authButton.setDisable(newValue.trim().isEmpty());
		});

		dialog.getDialogPane().setContent(gridPane);

		Platform.runLater(() -> apiKeyTextField.requestFocus());

		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == authButtonType) {
				return apiKeyTextField.getText();
			}
			return null;
		});

		Optional<String> result = dialog.showAndWait();

		if (result.isPresent()) {
			ecobeePane.initSession(ecobee);
			ecobeePane.loadDevicesFromSession();
		}
	}

	private boolean confirmPinRequest() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("PIN request");
		alert.setContentText("Request a PIN will require registering with the Ecobee App.  Do you wish to proceed?");
		Optional<ButtonType> result = alert.showAndWait();
		return result.get() == ButtonType.OK;
	}

	private static class AuthAccessorPasswordField extends PasswordField implements AuthAccessor {
	}

	private static class AuthAccessorTextField extends TextField implements AuthAccessor {
	}

	private void showUserPasswordDialog(TpLink tpLink, VendorPane tpLinkPane) {
		Dialog<Couple<String, String>> dialog = new Dialog<>();
		dialog.setTitle(new StringBuilder("Login: ").append(tpLink.getClass().getSimpleName()).toString());

		ButtonType loginButtonType = new ButtonType("Login", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

		GridPane gridPane = new GridPane();
		gridPane.setPadding(new Insets(5, 5, 5, 5));
		gridPane.setHgap(3);
		gridPane.setVgap(3);

		AuthAccessorTextField usernameTextField = new AuthAccessorTextField();
		usernameTextField.setPromptText("Username");
		tpLink.applyAuthCredential(usernameTextField, CredentialType.USERNAME);
		AuthAccessorPasswordField passwordTextField = new AuthAccessorPasswordField();
		passwordTextField.setPromptText("Password");
		tpLink.applyAuthCredential(passwordTextField, CredentialType.PASSWORD);

		gridPane.add(new Label("Username:"), 0, 0);
		gridPane.add(usernameTextField, 1, 0);
		gridPane.add(new Label("Password:"), 0, 1);
		gridPane.add(passwordTextField, 1, 1);

		Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
		loginButton.setDisable(true);

		usernameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			loginButton.setDisable(newValue.trim().isEmpty());
		});
		passwordTextField.textProperty().addListener((observable, oldValue, newValue) -> {
			loginButton.setDisable(newValue.trim().isEmpty());
		});

		dialog.getDialogPane().setContent(gridPane);

		Platform.runLater(() -> usernameTextField.requestFocus());

		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == loginButtonType) {
				return new Couple<String, String>(usernameTextField.getText(), passwordTextField.getText());
			}
			return null;
		});

		Optional<Couple<String, String>> result = dialog.showAndWait();

		if (result.isPresent()) {
			tpLink.setUsername(result.get().getValueOne());
			tpLink.setPassword(result.get().getValueTwo());

			tpLinkPane.initSession(tpLink);
			tpLinkPane.loadDevicesFromSession();
		}
	}

	@Override
	public void requestPersist(VendorApi<?> vendorApi) {
		executor.submit(persistTask);
	}

	@Override
	public void rescheduleUpdate(VendorApi<?> vendorApi, int updateInSeconds) {
		VendorPane targetVendorPane = vendorScehduldedUpdates.keySet().stream()
				.filter(vendorPane -> vendorPane.getVendorApi().equals(vendorApi)).findFirst().orElse(null);
		if (targetVendorPane != null) {
			if (updateInSeconds > 0) {
				vendorUpdateExecutor.schedule(() -> targetVendorPane.refreshAllDeviceStatuses(), updateInSeconds,
						TimeUnit.SECONDS);
			}
			sechuleUpdate(targetVendorPane);
		}
	}

	private void sechuleUpdate(VendorPane vendorPane) {
		ScheduledFuture<?> scehduled = vendorScehduldedUpdates.remove(vendorPane);
		if (scehduled != null) {
			scehduled.cancel(false);
		}
		Couple<Integer, Integer> interval = vendorPane.getVendorApi().getRefreshInterval();
		scehduled = vendorUpdateExecutor.scheduleAtFixedRate(() -> vendorPane.refreshAllDeviceStatuses(),
				interval.getValueOne(), interval.getValueTwo(), TimeUnit.MINUTES);
		vendorScehduldedUpdates.put(vendorPane, scehduled);
	}

	/**
	 * Main entry.
	 */
	public static void main(String[] args) {
		launch(args);
	}

}
