package dd.iot.example.desktop.vendor;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.Future;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.JsonNode;

import dd.iot.device.utils.Couple;
import dd.iot.device.utils.Utils;
import dd.iot.example.desktop.vendor.ui.VendorUi;
import dd.iot.model.Device;
import dd.iot.model.Device.State;
import dd.iot.transport.AuthAccessor;
import dd.iot.transport.AuthAccessor.CredentialType;
import dd.iot.transport.ErrorHandler;
import dd.iot.transport.ErrorHandler.ErrorResponse;
import dd.iot.transport.Session;
import dd.iot.transport.SessionManager;
import dd.iot.vendor.VendorApi;
import dd.iot.vendor.view.VendorView;
import javafx.scene.control.Label;

/**
 * @author danylukd
 *
 */
public final class EnvironmentCanada implements VendorApi<EnvironmentCanada>, VendorUi<EnvironmentCanada> {

	private final VendorView controller;

	private static final DateTimeFormatter timestampFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

	private static final int timeZoneOffsetHours = TimeZone.getDefault().getOffset(new Date().getTime()) / 3600000;
	
	private static final String DEFAULT_SITE_ID = "s0000430_e";

	List<Device> cachedDevices = new ArrayList<Device>();

	public EnvironmentCanada(VendorView controller) {
		this.controller = controller;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see dd.iot.transport.Authentication#authenticate()
	 */
	@Override
	public String authenticate(Session session) {
		return "no-session";
	}

	@Override
	public ErrorHandler getErrorHandler() {
		return null;
	}

	@Override
	public List<Device> getDevices(Session session) {
		// TODO: move this over to an XML parser (started off with simple requirements but now is getting ugly)
		LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);
		Device device = this.cachedDevices.stream().findFirst().orElse(null);
		String previousTimestamp = null;
		if (device != null) {
			previousTimestamp = Utils.valueOfNonNull(device.getStatusProperty("timestamp"));
		}

		device = new Device(DEFAULT_SITE_ID, null, null, null, getServiceUrl(), session.getSessionId());
		this.cachedDevices.clear();
		this.cachedDevices.add(device);
		
		String response = SessionManager.sendRaw(this, "GET", getServiceUrl(), null, null, null);
		Pattern pattern = Pattern.compile("<currentConditions>(.+?)</currentConditions>", Pattern.DOTALL);
		Matcher matcher = pattern.matcher(response);
		if (matcher.find()) {
			String currentConditions = matcher.group(1);
			String condition = getElementValue("<condition.*>(.+?)</condition>", currentConditions);
			String temperature = getElementValue("<temperature.*>(.+?)</temperature>", currentConditions);
			String humidity = getElementValue("<relativeHumidity.*>(.+?)</relativeHumidity>", currentConditions);

			device.setProperty("condition", condition);
			device.setProperty("temperature", temperature);
			device.setProperty("relativeHumidity", humidity);
			// timestamp
			pattern = Pattern.compile("<dateTime.*>(.+?)</dateTime>", Pattern.DOTALL);
			matcher = pattern.matcher(response);
			if (matcher.find()) {
				String dateTime = matcher.group(0);
				String updatedTimestamp = getElementValue("<timeStamp>(.+?)</timeStamp>", dateTime);
				device.setProperty("timestamp", updatedTimestamp);
				LocalDateTime timestamp = LocalDateTime.parse(updatedTimestamp, timestampFormatter);
				// we get updates after the top of the hour; if the last update was a previous hour or has not changed
				// at top of hour *and* the timestamp has not changed then we need to reschedule an update
				if ((timestamp.get(ChronoField.HOUR_OF_DAY) < now.get(ChronoField.HOUR_OF_DAY)
						|| now.get(ChronoField.MINUTE_OF_HOUR) < getUpdateInterval())
						&& updatedTimestamp.equals(previousTimestamp)) {
					this.controller.rescheduleUpdate(this, 60);
				}
			}
		}
		pattern = Pattern.compile("<forecastGroup>(.+?)</forecastGroup>", Pattern.DOTALL);
		matcher = pattern.matcher(response);
		StringBuilder forecast = new StringBuilder();
		int forecastCount = 0;
		if (matcher.find()) {
			String forcastGroup = matcher.group(0);
			pattern = Pattern.compile("(<forecast>(.*?)</forecast>)", Pattern.DOTALL);
			matcher = pattern.matcher(forcastGroup);
			while (matcher.find() && forecastCount++ < 4) {
				String forecastGroup = matcher.group(0);
				String period = getElementValue("<period.*>(.+?)</period>", forecastGroup);
				forecast.append(period).append(": ");
				Pattern forecastPattern = Pattern.compile("<temperatures>(.+?)</temperatures>", Pattern.DOTALL);
				Matcher forecastMatcher = forecastPattern.matcher(forecastGroup);
				if (forecastMatcher.find()) {
					String textSummary = getElementValue("<textSummary>(.+?)</textSummary>", forecastMatcher.group(0));
					forecast.append(textSummary);
				}
				forecastPattern = Pattern.compile("<abbreviatedForecast>(.+?)</abbreviatedForecast>", Pattern.DOTALL);
				forecastMatcher = forecastPattern.matcher(forecastGroup);
				if (forecastMatcher.find()) {
					String textSummary = getElementValue("<textSummary>(.+?)</textSummary>", forecastMatcher.group(0));
					forecast.append(" ").append(textSummary);
				}
				forecast.append("\n");
			}
		}

		pattern = Pattern.compile("<riseSet>(.+?)</riseSet>", Pattern.DOTALL);
		matcher = pattern.matcher(response);
		if (matcher.find()) {
			forecast.append("\u2600  ");
			String riseSet = matcher.group(0);

			Pattern riseSetPattern = Pattern.compile("<dateTime.name=\"sunrise\".zone=\"UTC\".*>(.+?)</dateTime>",
					Pattern.DOTALL);
			Matcher riseSetMatcher = riseSetPattern.matcher(riseSet);
			if (riseSetMatcher.find()) {
				String dateTime = riseSetMatcher.group(0);
				int hour = Integer.valueOf(getElementValue("<hour>(.+?)</hour>", dateTime)) + timeZoneOffsetHours;
				forecast.append("\u2191 ").append(hour).append(":")
						.append(getElementValue("<minute>(.+?)</minute>", dateTime)).append("AM");
			}

			riseSetPattern = Pattern.compile("<dateTime.name=\"sunset\".zone=\"UTC\".*>(.+?)</dateTime>",
					Pattern.DOTALL);
			riseSetMatcher = riseSetPattern.matcher(riseSet);
			if (riseSetMatcher.find()) {
				String dateTime = riseSetMatcher.group(0);
				int hour = Integer.valueOf(getElementValue("<hour>(.+?)</hour>", dateTime)) + timeZoneOffsetHours + 12;
				forecast.append("  \u2193 ").append(hour).append(":")
						.append(getElementValue("<minute>(.+?)</minute>", dateTime)).append("PM");
			}

		}
		device.setProperty("forecast", forecast.toString());
		return Collections.unmodifiableList(cachedDevices);
	}

	private static String getElementValue(String tag, String source) {
		Pattern pattern = Pattern.compile(tag, Pattern.DOTALL);
		Matcher matcher = pattern.matcher(source);
		if (matcher.find()) {
			return matcher.group(1);
		}
		return "";
	}

	@Override
	public Future<ErrorResponse> setDeviceState(Device device, State<?> state, String... params) {
		return ErrorHandler.createFuture(ErrorResponse.ERR_NONE);
	}

	@Override
	public String getServiceUrl() {
		// TODO: parse cities from https://dd.weather.gc.ca/citypage_weather/docs/site_list_en.csv
		// and create dropdown for selection
		return Utils.join("https://dd.weather.gc.ca/citypage_weather/xml/ON/", DEFAULT_SITE_ID, ".xml");
	}

	@Override
	public EnvironmentCanada loadInto(JsonNode data) {
		return this;
	}

	@Override
	public JsonNode saveOut() {
		return null;
	}

	@Override
	public boolean isDataOkay() {
		return true;
	}

	@Override
	public void applyAuthCredential(AuthAccessor accessor, CredentialType type) {
	}

	@Override
	public Function<Device, String> getDeviceMessage() {
		return device -> device != null ? Utils.join(device.getStatusProperty("condition"), ": ",
				device.getStatusProperty("temperature"), "° ",
				device.getStatusProperty("relativeHumidity"), "%") : "unknown";
	}

	@Override
	public Function<Device, String> getDeviceExtraMessage() {
		return device -> device != null ? device.getStatusProperty("forecast") : "unknown";
	}

	@Override
	public BiConsumer<Device, Label> applyDeviceMessageEffects() {
		return null;
	}

	private int getUpdateInterval() {
		return 10; // every 10 minutes to update
	}

	@Override
	public Couple<Integer, Integer> getRefreshInterval() {
		// the math here could be improved but it works
		final int interval = getUpdateInterval();
		final int startOffset = 1; // start 1 minute past interval
		final int currentTimeBased = LocalDateTime.now().get(ChronoField.MINUTE_OF_HOUR) + interval - startOffset;
		final int start = ((currentTimeBased / interval) * interval) - currentTimeBased + interval;
		return new Couple<Integer, Integer>(start, interval);
	}
}
