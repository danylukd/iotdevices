package dd.iot.example.desktop.vendor;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.function.BiConsumer;
import java.util.function.Function;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import dd.iot.device.utils.Couple;
import dd.iot.device.utils.Utils;
import dd.iot.example.desktop.vendor.ui.VendorUi;
import dd.iot.model.Device;
import dd.iot.model.Device.DeviceType;
import dd.iot.model.Device.State;
import dd.iot.transport.AuthAccessor;
import dd.iot.transport.AuthAccessor.CredentialType;
import dd.iot.transport.ErrorHandler;
import dd.iot.transport.ErrorHandler.ErrorResponse;
import dd.iot.transport.Session;
import dd.iot.transport.SessionManager;
import dd.iot.vendor.VendorApi;
import dd.iot.vendor.view.VendorView;
import javafx.scene.control.Label;

/**
 * @author danylukd
 *
 */
public final class TpLink implements VendorApi<TpLink>, VendorUi<TpLink> {

	public enum TpLinkState implements State<TpLinkState> {
		ON("1", "On"), OFF("0", "Off"), UNAVAILABLE("-1", "Unavailable");

		private String vendorState;
		private String message;

		/**
		 * Private constructor
		 * 
		 * @param state
		 *            the state as an integer
		 * @param message
		 *            the message for the state.
		 */
		private TpLinkState(String vendorState, String message) {
			this.vendorState = vendorState;
			this.message = message;
		}

		/**
		 * Returns the int of the state
		 * 
		 * @return
		 */
		public String getVendorState() {
			return this.vendorState;
		}

		/**
		 * Returns the message
		 * 
		 * @return
		 */
		public String getMessage() {
			return this.message;
		}
	}

	public enum TpLinkError implements ErrorHandler {
		error_code;
		private static final BiMap<Object, ErrorResponse> codesToErrorResponse = HashBiMap.create();
		static {
			codesToErrorResponse.put(0, ErrorResponse.ERR_NONE);
			codesToErrorResponse.put(-20651, ErrorResponse.ERR_TOKEN_EXPIRED);
			codesToErrorResponse.put(-10100, ErrorResponse.ERR_JSON_FORMAT);
			// -20104 --> "param doesn't exist" error
			codesToErrorResponse.put(-1, ErrorResponse.ERR_TIMEOUT);
			codesToErrorResponse.put(Integer.MAX_VALUE, ErrorResponse.ERR_UNKNOWN);
		}

		@Override
		public ErrorResponse getErrorResponse(Object reference) {
			return codesToErrorResponse.get(reference);
		}

		@Override
		public ErrorResponse isErrors(JsonNode deviceResponse) {
			if (deviceResponse.has(error_code.toString())) {
				ErrorResponse errorCode = getErrorResponse(deviceResponse.get(error_code.toString()).asInt());
				if (errorCode == null) {
					errorCode = ErrorResponse.ERR_UNKNOWN;
				}
				return errorCode;
			}
			return ErrorResponse.ERR_UNKNOWN;
		}

		@Override
		public String writeErrorJson(ErrorResponse errorResponse) {
			StringBuilder responseBody = new StringBuilder();
			responseBody.append("{\"").append(error_code.toString()).append("\":")
					.append(codesToErrorResponse.inverse().get(errorResponse)).append(",\"result\":{}}");
			return responseBody.toString();
		}
	}

	private enum TpLinkType implements DeviceType {
		HS100;

		@Override
		public State<TpLinkState> getState(Map<String, String> metadata) {
			if (metadata != null) {
				String relayState = Optional.ofNullable(metadata.get(JSON_ATTR_RELAY_STATE)).orElse("-1");
				return State.getState(TpLinkState.values(), TpLinkState.UNAVAILABLE,
						state -> relayState != null ? relayState.equals(state.getVendorState()) : false);
			}
			return TpLinkState.UNAVAILABLE;
		}

	}

	private static class TpLinkAccount {

		private String username;
		private String password;

		/**
		 * @return the username
		 */
		public String getUsername() {
			return username;
		}

		/**
		 * @param username
		 *            the username to set
		 */
		public void setUsername(String username) {
			this.username = username;
		}

		/**
		 * @return the password
		 */
		public String getPassword() {
			return password;
		}

		/**
		 * @param password
		 *            the password to set
		 */
		public void setPassword(String password) {
			this.password = password;
		}
	}

	private static final int UPDATE_INTERVAL_MINS = 15;
	private static final int INITIAL_UPDATE_DELAY_MINS = 1;

	private static final String JSON_ATTR_RELAY_STATE = "relay_state";

	private static final Map<String, String> tokenParams = new HashMap<String, String>();

	private TpLinkAccount account;

	public TpLink(VendorView controller) {
		account = new TpLinkAccount();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see dd.iot.transport.Authentication#authenticate()
	 */
	@Override
	public String authenticate(Session session) {

		StringBuilder authRequ = new StringBuilder(
				"{\"method\": \"login\",\"params\": {\"appType\": \"Kasa_Android\",");
		authRequ.append("\"cloudUserName\": \"").append(account.getUsername());
		authRequ.append("\",\"cloudPassword\": \"").append(account.getPassword());
		authRequ.append("\",\"terminalUUID\": \"MY_UUID_v4\"}}");
		JsonNode response = SessionManager.sendPost(this, getServiceUrl(), authRequ.toString(), null, null);

		if (TpLinkError.error_code.isErrors(response).isOk()) {
			String responseToken = response.get("result").get("token").asText();
			return responseToken;
		} else {
			session.setStatusMessage("No session: bad user/pwd");
		}
		return null;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.account.setUsername(username);
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		account.setPassword(password);
	}

	@Override
	public ErrorHandler getErrorHandler() {
		return TpLinkError.error_code;
	}

	@Override
	public List<Device> getDevices(Session session) {
		List<Device> devices = new ArrayList<Device>();
		JsonNode deviceResponse = SessionManager.sendPost(this, getServiceUrl(), "{\"method\":\"getDeviceList\"}",
				getTokenParams(session.getSessionId()), null);
		if (getErrorHandler().isErrors(deviceResponse).isOk()) {
			deviceResponse.get("result").get("deviceList").forEach(deviceJson -> {
				Device hs100 = new Device(deviceJson.get("deviceId").asText(), TpLinkType.HS100,
						deviceJson.get("deviceModel").asText(), deviceJson.get("alias").asText(),
						deviceJson.get("appServerUrl").asText(), session.getSessionId());
				devices.add(hs100);
				getDeviceInfo(hs100);
			});
		}
		
		return devices;
	}

	private Map<String, String> getTokenParams(UUID sessionId) {
		tokenParams.put("token", SessionManager.getToken(sessionId));
		return tokenParams;
	}

	@Override
	public Future<ErrorResponse> setDeviceState(Device device, State<?> state, String... params) {

		ErrorResponse returnResposne = ErrorResponse.ERR_NONE;
		if (device.getDeviceType() == TpLinkType.HS100) {
			StringBuilder switchStateRequest = new StringBuilder("{\"method\":\"passthrough\", \"params\": {");
			switchStateRequest.append("\"deviceId\": \"").append(device.getId()).append("\"");
			// request data is quoted in JSON
			switchStateRequest.append(", \"requestData\": \"{\\\"system\\\":{\\\"set_relay_state\\\":{");
			switchStateRequest.append("\\\"state\\\":").append(String.valueOf(state.getVendorState()));
			switchStateRequest.append("}}}\"}}");

			JsonNode response = SessionManager.sendPost(this, device, switchStateRequest.toString(),
					getTokenParams(device.getSessionId()), null);

			if (getErrorHandler().isErrors(response) == ErrorResponse.ERR_TOKEN_EXPIRED) {
				SessionManager.refreshSession(device.getSessionId());
				response = SessionManager.sendPost(this, device, switchStateRequest.toString(),
						getTokenParams(device.getSessionId()), null);
				if (!getErrorHandler().isErrors(response).isOk()) {
					returnResposne = getErrorHandler().isErrors(response);
				}
			}
		} else {
			returnResposne = ErrorResponse.ERR_UNKNOWN;
		}

		return ErrorHandler.createFuture(returnResposne);
	}

	private void getDeviceInfo(Device device) {

		StringBuilder infoRequest = new StringBuilder();
		infoRequest.append("{\"method\":\"passthrough\", \"params\": {");
		infoRequest.append("\"deviceId\": \"").append(device.getId());
		// request data is quoted in JSON
		infoRequest.append("\", \"requestData\":")
				.append("\"{\\\"system\\\":{\\\"get_sysinfo\\\":null},\\\"emeter\\\":{\\\"get_realtime\\\":null}}\"}}");
		JsonNode response = SessionManager.sendPost(this, device, infoRequest.toString(),
				getTokenParams(device.getSessionId()), null);
		Map<String, String> info = new HashMap<String, String>();
		if (getErrorHandler().isErrors(response) == ErrorResponse.ERR_TOKEN_EXPIRED) {
			SessionManager.refreshSession(device.getSessionId());
			response = SessionManager.sendPost(this, device, infoRequest.toString(),
					getTokenParams(device.getSessionId()), null);
		}

		if (getErrorHandler().isErrors(response).isOk()) {
			String result = response.get("result").get("responseData").toString().replace("\\", "");
			result = result.substring(1, result.length() - 1); // remove first/last quotes
			try {
				JsonNode responseData = new ObjectMapper().readTree(result.toString());
				JsonNode systemInfo = responseData.get("system").get("get_sysinfo");
				info = new ObjectMapper().readValue(systemInfo.toString(), new TypeReference<Map<String, String>>() {
				});
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			info.put(JSON_ATTR_RELAY_STATE, "-1");
		}
		device.setProperties(info, true);
	}

	@Override
	public String getServiceUrl() {
		return "https://wap.tplinkcloud.com/";
	}

	@Override
	public TpLink loadInto(JsonNode data) {
		if (data != null) {
			try {
				account = new ObjectMapper().readValue(data.toString(), TpLinkAccount.class);
				account.setPassword(crypto(account.getUsername(), account.getPassword(), false));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return this;
	}

	/**
	 * This is to make sure that we are not going to export/import password data in clear text. Granted, not *the* most
	 * secure but an additional layer of comfort. Users of this API should modify this for their own needs.
	 * 
	 * @param first
	 * @param second
	 * @param encode
	 * @return
	 */
	private static String crypto(String first, String second, boolean encode) {
		if (second != null) {
			int i = 0;
			byte[] firstBytes = first.getBytes();
			byte[] secondBytes = second.getBytes();
			if (!encode) {
				secondBytes = Base64.getDecoder().decode(secondBytes);
			}
			while (i < firstBytes.length && i < secondBytes.length) {
				secondBytes[i] = (byte) (firstBytes[i] ^ secondBytes[i++]);
			}
			// add/remove ending random junk values
			secondBytes = Arrays.copyOf(secondBytes, secondBytes.length + 5 * (encode ? 1 : -1));
			if (encode) {
				// add random junk values at end of encoded value
				SecureRandom random = new SecureRandom();
				byte[] randBytes = new byte[5];
				random.nextBytes(randBytes);
				for (i = secondBytes.length - 5; i < secondBytes.length; i++) {
					secondBytes[i] = randBytes[secondBytes.length - i - 1];
				}
				second = new String(Base64.getEncoder().encode(secondBytes));
			} else {
				second = new String(secondBytes);
			}
		}
		return second;
	}

	@Override
	public JsonNode saveOut() {
		final String passwordTemp = account.getPassword();
		account.setPassword(crypto(account.getUsername(), passwordTemp, true));
		JsonNode data = new ObjectMapper().convertValue(account, JsonNode.class);
		account.setPassword(passwordTemp); // set it back so we can use it
		return data;
	}

	@Override
	public boolean isDataOkay() {
		return account != null && account.getUsername() != null && !account.getUsername().isEmpty()
				&& account.getPassword() != null && !account.getPassword().isEmpty();
	}

	@Override
	public void applyAuthCredential(AuthAccessor accessor, CredentialType type) {
		switch (type) {
		case USERNAME:
			accessor.setText(account.getUsername());
			break;
		case PASSWORD:
			accessor.setText(account.getPassword());
			break;
		default: // do nothing
		}
	}

	@Override
	public Function<Device, String> getDeviceMessage() {
		return device -> Utils.join(device != null ? device.getAlias() : "None", ": ",
				device != null ? device.getState().getMessage() : "unknown");
	}

	@Override
	public Function<Device, String> getDeviceExtraMessage() {
		return null;
	}

	@Override
	public BiConsumer<Device, Label> applyDeviceMessageEffects() {
		return null;
	}

	@Override
	public Couple<Integer, Integer> getRefreshInterval() {
		return new Couple<Integer, Integer>(UPDATE_INTERVAL_MINS, INITIAL_UPDATE_DELAY_MINS);
	}
}
