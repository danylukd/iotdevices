package dd.iot.example.desktop.vendor.ui;

import java.util.function.BiConsumer;
import java.util.function.Function;

import dd.iot.model.Device;
import javafx.scene.control.Label;

public interface VendorUi<T> {

	public BiConsumer<Device, Label> applyDeviceMessageEffects();

	public Function<Device, String> getDeviceMessage();

	public Function<Device, String> getDeviceExtraMessage();

}
