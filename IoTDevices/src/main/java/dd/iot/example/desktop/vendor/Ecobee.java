package dd.iot.example.desktop.vendor;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Function;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import dd.iot.device.utils.Couple;
import dd.iot.device.utils.Utils;
import dd.iot.example.desktop.vendor.ui.VendorUi;
import dd.iot.model.Device;
import dd.iot.model.Device.DeviceType;
import dd.iot.model.Device.State;
import dd.iot.transport.AuthAccessor;
import dd.iot.transport.AuthAccessor.CredentialType;
import dd.iot.transport.ErrorHandler;
import dd.iot.transport.ErrorHandler.ErrorResponse;
import dd.iot.transport.Session;
import dd.iot.transport.SessionManager;
import dd.iot.vendor.VendorApi;
import dd.iot.vendor.view.VendorView;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 * @author danylukd
 *
 */
public class Ecobee implements VendorApi<Ecobee>, VendorUi<Ecobee> {

	public enum EcobeeMode implements State<EcobeeMode> {
		// TODO: refactor so the state is subset of mode (instead of this)
		OFF(EcobeeState.OFF, true), HEAT(EcobeeState.ON_HEAT, true), COOL(EcobeeState.ON_COOL,
				true), UNKNOWN(EcobeeState.UNAVAILABLE, false);

		private EcobeeState correspondingState;
		private boolean settable;

		private EcobeeMode(EcobeeState correspondingState, boolean settable) {
			this.correspondingState = correspondingState;
			this.settable = settable;
		}

		/**
		 * @return the correspondingState
		 */
		public EcobeeState getCorrespondingState() {
			return correspondingState;
		}

		@Override
		public String toString() {
			return correspondingState.getMessage();
		}

		public static EcobeeMode getEcobeeMode(String mode) {
			for (EcobeeMode ecobeeMode : values()) {
				if (ecobeeMode.correspondingState.getMessage().equalsIgnoreCase(mode)) {
					return ecobeeMode;
				}
			}
			return null;
		}

		/**
		 * @return the settable
		 */
		public boolean isSettable() {
			return settable;
		}

		@Override
		public String getVendorState() {
			return correspondingState.getVendorState();
		}

		@Override
		public String getMessage() {
			return correspondingState.getMessage();
		}
	}

	public enum EcobeeState implements State<EcobeeState> {
		ON_HEAT("heat", "Heat"), ON_COOL("compCool", "Cool"), OFF("", "Off"), TEMPERATURE_UP("up",
				"+"), TEMPERATURE_DOWN("down",
						"-"), TEMPERATURE_RESUME("resume", "Cancel"), UNAVAILABLE("", "Unavailable");

		private String vendorState;
		private String message;

		/**
		 * Private constructor
		 * 
		 * @param state
		 *            the state as an integer
		 * @param message
		 *            the message for the state.
		 */
		private EcobeeState(String vendorState, String message) {
			this.vendorState = vendorState;
			this.message = message;
		}

		/**
		 * Returns the int of the state
		 * 
		 * @return
		 */
		public String getVendorState() {
			return this.vendorState;
		}

		/**
		 * Returns the message
		 * 
		 * @return
		 */
		public String getMessage() {
			return this.message;
		}
	}

	public enum EcobeeError implements ErrorHandler {
		status;
		private static final BiMap<Object, ErrorResponse> codesToErrorResponse = HashBiMap.create();
		static {
			codesToErrorResponse.put(0, ErrorResponse.ERR_NONE);
			codesToErrorResponse.put(14, ErrorResponse.ERR_TOKEN_EXPIRED);
			codesToErrorResponse.put(4, ErrorResponse.ERR_JSON_FORMAT);
			codesToErrorResponse.put(-1, ErrorResponse.ERR_TIMEOUT);
			codesToErrorResponse.put(1, ErrorResponse.ERR_BAD_AUTH);// "Authentication failed. Token is required."
																	// means no auth in header
			codesToErrorResponse.put(Integer.MAX_VALUE, ErrorResponse.ERR_UNKNOWN);
		}

		@Override
		public ErrorResponse getErrorResponse(Object reference) {
			return codesToErrorResponse.get(reference);
		}

		@Override
		public ErrorResponse isErrors(JsonNode deviceResponse) {
			if (deviceResponse.has(EcobeeError.status.toString())) {
				ErrorResponse errorCode = getErrorResponse(
						deviceResponse.get(EcobeeError.status.toString()).get("code").asInt());
				return errorCode;
			}
			return ErrorResponse.ERR_NONE;
		}

		@Override
		public String writeErrorJson(ErrorResponse errorResponse) {
			StringBuilder responseBody = new StringBuilder();
			responseBody.append("{\"").append(status.toString()).append("\":{\"code\":")
					.append(codesToErrorResponse.inverse().get(errorResponse)).append("}}");
			return responseBody.toString();
		}
	}

	public enum EcobeeType implements DeviceType {
		THERMOSTAT {
			@Override
			public State<?> getState(Map<String, String> metadata) {
				String status = Utils.valueOfNonNull(metadata.get("status")).toLowerCase();
				return State.getState(EcobeeState.values(), EcobeeState.UNAVAILABLE,
						state -> status != null ? status.contains(state.getVendorState().toLowerCase()) : false);
			}
		},

		SENSOR {
			@Override
			public State<?> getState(Map<String, String> metadataz) {
				return EcobeeState.UNAVAILABLE;
			}
		};
	}

	private static class EcobeeAuth {

		private String apiKey, code, token, refreshToken, pin;

		private EcobeeAuth() {
		}

		/**
		 * @return the apiKey
		 */
		public String getApiKey() {
			return apiKey;
		}

		/**
		 * @param apiKey
		 *            the apiKey to set
		 */
		public void setApiKey(String apiKey) {
			this.apiKey = apiKey;
		}

		/**
		 * @return the code
		 */
		public String getCode() {
			return code;
		}

		/**
		 * @param code
		 *            the code to set
		 */
		public void setCode(String code) {
			this.code = code;
		}

		/**
		 * @return the token
		 */
		public String getToken() {
			return token;
		}

		/**
		 * @param token
		 *            the token to set
		 */
		public void setToken(String token) {
			this.token = token;
		}

		/**
		 * @return the refreshToken
		 */
		public String getRefreshToken() {
			return refreshToken;
		}

		/**
		 * @param refreshToken
		 *            the refreshToken to set
		 */
		public void setRefreshToken(String refreshToken) {
			this.refreshToken = refreshToken;
		}

		/**
		 * @return the pin
		 */
		public String getPin() {
			return pin;
		}

		/**
		 * @param pin
		 *            the pin to set
		 */
		public void setPin(String pin) {
			this.pin = pin;
		}

		@JsonIgnore
		public boolean isRequiresToken() {
			return this.token == null || this.refreshToken == null;
		}
	}

	private static final int UPDATE_INTERVAL_MINS = 3;
	private static final int INITIAL_UPDATE_DELAY_MINS = 3;

	private static final int STATE_CHANGE_UPDATE_POLL_SECS = 30;
	private static final int STATE_CHANGE_QUEUE_TIMER_SECS = 3;

	private static int HALF_DEGREE_CELCIUS_IN_FARENHEIT = 9;

	private final Map<String, String> headers = new HashMap<String, String>();

	private EcobeeAuth auth;

	private final VendorView controller;

	private List<Device> cachedDevices = new ArrayList<Device>();

	private LinkedBlockingDeque<String> temperatureQueue = new LinkedBlockingDeque<>(10);
	private Future<ErrorResponse> temperatureFuture;
	private final ScheduledExecutorService scheduledExecutor;

	public Ecobee(VendorView controller, ScheduledExecutorService scheduledExecutor) {
		auth = new EcobeeAuth();
		this.controller = controller;
		this.scheduledExecutor = scheduledExecutor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see dd.iot.transport.Authentication#authenticate()
	 */
	@Override
	public String authenticate(Session session) {
		String results = null;
		// if we have code then we can proceed
		if (this.auth.getCode() != null) {
			Map<String, String> urlParams = new HashMap<String, String>();
			urlParams.put("client_id", this.auth.getApiKey());
			// if we have a refresh token then get a new token
			if (this.auth.getRefreshToken() != null) {
				urlParams.put("grant_type", "refresh_token");
				urlParams.put("refresh_token", this.auth.getRefreshToken());
			} else {
				// else, we need to get a refresh token
				urlParams.put("grant_type", "ecobeePin");
				urlParams.put("code", this.auth.getCode());
			}
			String uri = new StringBuilder(getServiceUrl()).append("token").toString();
			urlParams.put("client_id", this.auth.getApiKey());

			JsonNode response = SessionManager.sendPost(this, uri, "", urlParams, null);
			if (response.has("access_token") && response.has("refresh_token")) {
				this.auth.setRefreshToken(response.get("refresh_token").asText());
				this.auth.setToken(response.get("access_token").asText());
			}
			if (response.has("error")) {
				results = response.get("error").asText(); // returns "invalid_grant" ...means redo PIN
			} else {
				results = response.get("access_token").asText();
				this.controller.requestPersist(this);
			}
		}
		return results;
	}

	public String requestPin() {
		this.auth.setCode(null);
		this.auth.setPin(null);
		this.auth.setRefreshToken(null);
		this.auth.setToken(null);

		Map<String, String> urlParams = new HashMap<String, String>();
		urlParams.put("response_type", "ecobeePin");
		urlParams.put("scope", "smartWrite");
		urlParams.put("client_id", this.auth.getApiKey());

		JsonNode response = SessionManager.sendGet(this,
				new StringBuilder(getServiceUrl()).append("authorize").toString(), urlParams, null);

		if (EcobeeError.status.isErrors(response).isOk()) {
			this.auth.setCode(response.get("code").asText());
			this.auth.setPin(response.get("ecobeePin").asText());
		}
		return this.auth.getPin();
	}

	private JsonNode sendGetEcobee(String uri, Map<String, String> urlParams) {
		if (uri == null) {
			uri = "";
		}
		JsonNode results = SessionManager.sendGet(this, new StringBuilder(getServiceUrl()).append(uri).toString(),
				urlParams, getHeaders());
		return results;
	}

	private JsonNode sendPostEcobee(String uri, String payload, Map<String, String> urlParams) {
		if (uri == null) {
			uri = "";
		}
		JsonNode results = SessionManager.sendPost(this, new StringBuilder(getServiceUrl()).append(uri).toString(),
				payload, urlParams, getHeaders());
		return results;
	}

	private Map<String, String> getHeaders() {
		if (this.auth.getToken() != null) {
			headers.put("Authorization", new StringBuilder("Bearer ").append(this.auth.getToken()).toString());
		}
		return headers;
	}

	@Override
	public ErrorHandler getErrorHandler() {
		return EcobeeError.status;
	}

	@Override
	public List<Device> getDevices(Session session) {
		if (session != null) {
			List<Device> updatedDevices = getDevicesInternal(session.getSessionId());
			this.cachedDevices = updatedDevices;
		}
		return Collections.unmodifiableList(this.cachedDevices);
	}

	private List<Device> getDevicesInternal(UUID sessionId) {
		Instant now = Instant.now();
		Map<String, String> urlParams = new HashMap<String, String>();
		urlParams.put("format", "json");
		urlParams.put("body",
				"{%22selection%22:{%22selectionType%22:%22registered%22,%22selectionMatch%22:%22%22,%22includeRuntime%22:true,%22includeSensors%22:true,%22includeSettings%22:true}}");
		JsonNode results = sendGetEcobee("1/thermostat", urlParams);

		List<Device> devices = new ArrayList<Device>();
		int statusCode = results.get("status").get("code").asInt();
		if (EcobeeError.status.getErrorResponse(statusCode) == ErrorResponse.ERR_TOKEN_EXPIRED) {
			SessionManager.refreshSession(sessionId);
			results = sendGetEcobee("1/thermostat", urlParams);
		}
		statusCode = results.get("status").get("code").asInt();
		Device thermostat = null;
		if (EcobeeError.status.getErrorResponse(statusCode).isOk()) {
			JsonNode thermostatNode = results.get("thermostatList").elements().next();
			Iterator<JsonNode> elements = thermostatNode.get("remoteSensors").elements();
			while (elements.hasNext()) {
				JsonNode deviceJson = elements.next();
				Device device = new Device(deviceJson.get("id").asText(),
						deviceJson.get("type").asText().equalsIgnoreCase("thermostat") ? EcobeeType.THERMOSTAT
								: EcobeeType.SENSOR,
						deviceJson.get("type").asText(), deviceJson.get("name").asText(), getServiceUrl(), sessionId);
				if (device.getDeviceType() == EcobeeType.THERMOSTAT && thermostatNode.has("settings")) {
					device.setProperty("hvacMode",
							thermostatNode.get("settings").get("hvacMode").asText());
					thermostat = device;
				}
				Iterator<JsonNode> capabilities = deviceJson.get("capability").elements();
				while (capabilities.hasNext()) {
					JsonNode capability = capabilities.next();
					String type = Optional.of(capability.get("type").asText()).orElse("");
					if (type.equals("temperature")) {
						device.setProperty("temperature", capability.get("value").asText());
					} else if (type.equals("occupancy")) {
						device.setProperty("occupancy", capability.get("value").asText());
					} else if (type.equals("humidity")) {
						device.setProperty("humidity", capability.get("value").asText());
					}
				}

				device.setProperty("updated", now.toString());
				devices.add(device);
			}

			if (thermostat != null) {
				urlParams.put("body",
						"{%22selection%22:{%22selectionType%22:%22registered%22,%22selectionMatch%22:%22%22,%22includeEquipmentStatus%22:true}}");
				results = sendGetEcobee("1/thermostatSummary", urlParams);

				if (results.get("statusList").size() == 1) {
					String status[] = results.get("statusList").get(0).asText().split(":");
					if (status.length > 1) {
						String[] mode = status[1].split(",");
						thermostat.setProperty("status", mode.length > 1 ? mode[0] : "");
					}
				}

				JsonNode runtime = thermostatNode.get("runtime");
				thermostat.setProperty("desiredHeat",
						baselineFahrenheitTemp(runtime.get("desiredHeat").asText()));
				thermostat.setProperty("desiredCool",
						baselineFahrenheitTemp(runtime.get("desiredCool").asText()));
			}
		}
		return devices;
	}

	private static String baselineFahrenheitTemp(String decaFahrenheit) {
		if (Utils.valueOfNonNull(decaFahrenheit).isEmpty()) {
			return "";
		}
		// need to ensure that we're able to convert +/-0.5 Celsius from Farenheit
		int rawFar = ((Integer.valueOf(decaFahrenheit) - 14) / 9) * 9 + 14; // let rounding do the work
		decaFahrenheit = String.valueOf(rawFar);
		return decaFahrenheit;
	}

	@Override
	public Future<ErrorResponse> setDeviceState(Device device, State<?> state, String... params) {
		if (device != null && state != null) {
			String newTemp = Utils.valueOfNonNull(device.getStatusProperty("newTemp"));
			if (newTemp.isEmpty()) {
				newTemp = "cool".equals(device.getStatusProperty("hvacMode"))
						? device.getStatusProperty("desiredCool")
						: device.getStatusProperty("desiredHeat");
			}
			String payload = "{\"selection\": {\"selectionType\":\"registered\",\"selectionMatch\":\"\"},";
			String typeAttribute = null, paramsAttributes = null;
			int tempFloat = newTemp != null ? Integer.valueOf(newTemp) : 0;
			if (state == EcobeeState.TEMPERATURE_UP) {
				tempFloat += HALF_DEGREE_CELCIUS_IN_FARENHEIT;
				typeAttribute = "setHold";
			} else if (state == EcobeeState.TEMPERATURE_DOWN) {
				tempFloat -= HALF_DEGREE_CELCIUS_IN_FARENHEIT;
				typeAttribute = "setHold";
			} else if (state == EcobeeState.TEMPERATURE_RESUME) {
				typeAttribute = "resumeProgram";
				paramsAttributes = "\"resumeAll\":false";
			}
			if (state instanceof EcobeeState) {
				if (typeAttribute.equals("setHold")) {
					newTemp = String.valueOf(tempFloat);
					paramsAttributes = String.format(
							"\"holdType\":\"nextTransition\",\"heatHoldTemp\":%s,\"coolHoldTemp\":%s", newTemp,
							newTemp);
				}
				device.setProperty("newTemp", newTemp);
				payload = Utils.join(payload, "\"functions\": [{\"type\":\"", typeAttribute, "\",\"params\":{",
						paramsAttributes, "}}]}");
			} else if (state instanceof EcobeeMode) {
				payload = Utils.join(payload, "\"thermostat\": { \"settings\":{ \"hvacMode\":\"",
						state.getMessage().toLowerCase(), "\" } } }");
			} else {
				payload = null;
			}
			if (payload != null) {
				try {
					temperatureQueue.put(payload);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (this.temperatureFuture == null || this.temperatureFuture.isDone()) {
					this.temperatureFuture = this.scheduledExecutor.schedule(() -> sendTemperatureChange(device),
							STATE_CHANGE_QUEUE_TIMER_SECS, TimeUnit.SECONDS);
				}
			}
		}
		return this.temperatureFuture;
	}

	private ErrorResponse sendTemperatureChange(Device device) {
		if (temperatureQueue.isEmpty()) {
			return ErrorResponse.ERR_UNKNOWN;
		}
		String payload = temperatureQueue.getLast();
		if (payload == null) {
			return ErrorResponse.ERR_UNKNOWN;
		}
		this.temperatureQueue.clear();
		Map<String, String> urlParams = new HashMap<String, String>();
		urlParams.put("format", "json");
		JsonNode results = sendPostEcobee("1/thermostat", payload, urlParams);

		int statusCode = results.get("status").get("code").asInt();
		if (EcobeeError.status.getErrorResponse(statusCode) == ErrorResponse.ERR_TOKEN_EXPIRED) {
			SessionManager.refreshSession(device.getSessionId());
			results = sendPostEcobee("1/thermostat", payload, urlParams);
			statusCode = results.get("status").get("code").asInt();
		}

		if (EcobeeError.status.getErrorResponse(statusCode).isOk()) {
			this.controller.rescheduleUpdate(this, STATE_CHANGE_UPDATE_POLL_SECS);
		} else {
			return EcobeeError.status.getErrorResponse(statusCode);
		}
		this.temperatureFuture = null;

		return ErrorResponse.ERR_NONE;
	}

	@Override
	public String getServiceUrl() {
		return "https://api.ecobee.com/";
	}

	@Override
	public Ecobee loadInto(JsonNode data) {
		if (data != null) {
			try {
				this.auth = new ObjectMapper().readValue(data.toString(), EcobeeAuth.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return this;
	}

	@Override
	public JsonNode saveOut() {
		JsonNode data = new ObjectMapper().convertValue(this.auth, JsonNode.class);
		return data;
	}

	@Override
	public boolean isDataOkay() {
		return !this.auth.isRequiresToken();
	}

	@Override
	public void applyAuthCredential(AuthAccessor accessor, CredentialType type) {
		// TODO: should use this method from UI
	}

	/**
	 * @return the apiKey
	 */
	public String getApiKey() {
		return this.auth.getApiKey();
	}

	/**
	 * @param apiKey
	 *            the apiKey to set
	 */
	public void setApiKey(String apiKey) {
		this.auth.setApiKey(apiKey);
	}

	public String getPin() {
		return this.auth.getPin();
	}

	@Override
	public Function<Device, String> getDeviceMessage() {
		return device -> getDeviceMessageInternal(device);
	}

	private String getDeviceMessageInternal(Device device) {
		if (device == null) {
			return "None: unknown status";
		} else {
			String humdity = Utils.valueOfNonNull(device.getStatusProperty("humidity"));
			return Utils.join(device.getAlias(), ": ",
					Utils.getInCelsius(device.getStatusProperty("temperature")), "° ",
					!humdity.isEmpty() ? Utils.join(humdity, "%") : "");
		}
	}

	@Override
	public Function<Device, String> getDeviceExtraMessage() {
		return null;
	}

	@Override
	public BiConsumer<Device, Label> applyDeviceMessageEffects() {
		return (device, label) -> applyDeviceMessageEffects(device, label);
	}

	private void applyDeviceMessageEffects(Device device, Label label) {
		final Font font = Font.getDefault();
		label.setFont(Boolean.parseBoolean(device.getStatusProperty("occupancy"))
				? (Font.font(font.getFamily(), FontWeight.BOLD, font.getSize())) : font);
		State<?> state = device.getState();
		EcobeeState ecobeeEtate = null;
		if (state instanceof EcobeeState) {
			ecobeeEtate = (EcobeeState) device.getState();
		} else {
			ecobeeEtate = EcobeeState.UNAVAILABLE;
		}
		switch (ecobeeEtate) {
		case ON_HEAT:
			label.setTextFill(Color.DARKRED);
			break;
		case ON_COOL:
			label.setTextFill(Color.MEDIUMBLUE);
			break;
		case OFF:
		default:
			label.setTextFill(Color.BLACK); // Label class defaults to black so stick with it
			break;
		}
	}

	@Override
	public Couple<Integer, Integer> getRefreshInterval() {
		return new Couple<Integer, Integer>(UPDATE_INTERVAL_MINS, INITIAL_UPDATE_DELAY_MINS);
	}
}
