/**
 * 
 */
package dd.iot.example.desktop.vendor.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.Function;

import com.fasterxml.jackson.databind.JsonNode;

import dd.iot.device.utils.Utils;
import dd.iot.model.Device;
import dd.iot.transport.Session;
import dd.iot.transport.SessionManager;
import dd.iot.vendor.VendorApi;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

/**
 * @author danylukd
 *
 */
public class VendorPane extends GridPane {
	private VendorApi<?> vendorApiImpl;
	private VendorUi<?> vendorUiImpl;
	private Session session;

	private Map<Device, Label> deviceLabelsByDevice = new HashMap<Device, Label>();
	private GridPane devicesPane = new GridPane();

	private static final String TOOLTIP_CSS = Utils.join(
			"-fx-background-color: lightyellow; -fx-text-fill: black; -fx-font-size: ",
			((int) Font.getDefault().getSize()) + 2, "px;");

	public VendorPane(VendorApi<?> vendorImpl, VendorUi<?> vendorUiImpl) {
		this.vendorApiImpl = vendorImpl;
		this.vendorUiImpl = vendorUiImpl;
		add(devicesPane, 0, 0);
	}

	public void setupFromData(JsonNode deviceData) {
		// let vendor load them
		vendorApiImpl.loadInto(deviceData);
		if (vendorApiImpl.isDataOkay()) {
			CompletableFuture.supplyAsync(() -> initSession(vendorApiImpl))
					.thenCompose(session -> CompletableFuture.runAsync(() -> loadDevicesFromSession()));
		} else {
			Label statusLabel = new Label("Could not load data from file.");
			devicesPane.add(statusLabel, 0, 0);
		}
	}

	public Session initSession(VendorApi<?> vendor) {
		this.session = SessionManager.create(vendor);
		this.session.refreshToken();
		return this.session;
	}

	public void loadDevicesFromSession() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				ObservableList<Device> options = null;
				devicesPane.getChildren().clear();
				if (session.getToken() != null) {
					List<Device> devices = vendorApiImpl.getDevices(session);
					if (devices.isEmpty()) {
						devices.add(Device.getUnknownDevice());
					}
					options = FXCollections.observableArrayList(devices);
					deviceLabelsByDevice = new HashMap<Device, Label>();
					int row = 0;
					for (Device device : devices) {
						addDeviceStatusLabel(device, row++);
					}
				} else {
					Label statusLabel = new Label(session.getStatusMessage());
					devicesPane.add(statusLabel, 0, 0);
				}
				if (options != null) {
					deviceLabelsByDevice.keySet()
							.forEach(device -> updateDeviceStatusLabels(device, vendorUiImpl.getDeviceMessage(),
									vendorUiImpl.getDeviceExtraMessage(), vendorUiImpl.applyDeviceMessageEffects()));
					updateDevicesAndStatus(options);
				}
			}
		});
	}

	private void addDeviceStatusLabel(Device device, int row) {
		Label statusLabel = new Label("Status: Loading...");
		statusLabel.setAlignment(Pos.CENTER_LEFT);
		statusLabel.setTextAlignment(TextAlignment.LEFT);
		statusLabel.setWrapText(true);
		deviceLabelsByDevice.put(device, statusLabel);
		devicesPane.add(deviceLabelsByDevice.get(device), 0, row++);
	}

	/**
	 * Can be overridden for addition UI setup behaviour. Will be called after session is created and device(s) loaded
	 * from {@linkplain #loadDevicesFromSession()}.
	 * 
	 * @param options
	 *            the list of {@linkplain Device} and collection will be guaranteed to be non-null.
	 */
	public void updateDevicesAndStatus(ObservableList<Device> options) {
	}

	public void refreshAllDeviceStatuses() {
		final List<Device> devices = vendorApiImpl.getDevices(this.session);

		final VendorUi<?> vendorUi = this.vendorUiImpl;
		Platform.runLater(() -> devices.forEach(device -> updateDeviceStatusLabels(device,
				vendorUi.getDeviceMessage(), vendorUi.getDeviceExtraMessage(), vendorUi.applyDeviceMessageEffects())));
	}

	public void updateDeviceStatusLabels(Device device, Function<Device, String> message,
			Function<Device, String> extraMessage, BiConsumer<Device, Label> messageEffects) {
		String deviceMessge = message.apply(device);
		Device cachedDevice = deviceLabelsByDevice.keySet().stream()
				.filter(dev -> dev.getId()!= null && dev.getId().equals(device.getId())).findFirst().orElse(Device.getUnknownDevice());
		Label deviceLabel = deviceLabelsByDevice.get(cachedDevice);
		deviceLabel.setText(deviceMessge);
		if (extraMessage != null) {
			Tooltip tooltip = deviceLabel.getTooltip();
			if (tooltip == null) {
				tooltip = new Tooltip();
				Utils.hackTooltipStartTiming(tooltip, 0, 10000, 3000);
				tooltip.setStyle(TOOLTIP_CSS);
				deviceLabel.setTooltip(tooltip);
			}
			tooltip.setText(extraMessage.apply(device));
		}
		if (messageEffects != null) {
			messageEffects.accept(device, deviceLabel);
		}
	}

	/**
	 * @return the vendor API
	 */
	public VendorApi<?> getVendorApi() {
		return vendorApiImpl;
	}

	public Set<Device> getDevices() {
		return deviceLabelsByDevice.keySet();
	}

}