package dd.iot.transport;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import javax.net.ssl.HttpsURLConnection;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import dd.iot.model.Device;
import dd.iot.transport.ErrorHandler.ErrorResponse;
import dd.iot.vendor.VendorApi;

/**
 * A session for accessing IOT devices, done as a singleton.
 * 
 * @author Darcy Danyluk
 *
 */
public final class SessionManager {

	// timeout any after 10 seconds... which seems long in this day & age, anyways
	private static final int TIMEOUT = 10000;

	private static final boolean DEBUG = false; // TODO look into better frameworks for this

	private final static Map<UUID, Session> sessions = new HashMap<UUID, Session>();
	private static final Lock lock = new ReentrantLock();

	public static Session create(VendorApi<?> vednorApi) {
		lock.lock();
		Session instance = null;
		try {
			UUID sessionid = UUID.randomUUID();
			instance = new Session(vednorApi, sessionid);
			sessions.put(sessionid, instance);
		} finally {
			lock.unlock();
		}
		return instance;
	}

	/**
	 * Private constructor.
	 * 
	 */
	private SessionManager() {
	}

	private static Session getSession(UUID sessionId) {
		lock.lock();
		Session instance = null;
		try {
			instance = sessions.get(sessionId);
		} finally {
			lock.unlock();
		}
		return instance;
	}

	public static void refreshSession(UUID sessionId) {
		Session session = getSession(sessionId);
		if (session != null) {
			session.refreshToken();
		}
	}

	public static String getToken(UUID sessionId) {
		return getSession(sessionId).getToken();
	}

	public static JsonNode sendPost(VendorApi<?> vendor, Device device, String payload, Map<String, String> urlParams,
			Map<String, String> headers) {
		return sendPost(vendor, device.getAppServerUrl(), payload, urlParams, headers);
	}

	public static JsonNode sendGet(VendorApi<?> vendor, Device device, Map<String, String> urlParams,
			Map<String, String> headers) {
		return sendGet(vendor, device.getAppServerUrl(), urlParams, headers);
	}

	public static JsonNode sendPost(VendorApi<?> vendor, String requestUrl, String payload,
			Map<String, String> urlParams, Map<String, String> headers) {
		return send(vendor, "POST", requestUrl, payload, urlParams, headers);
	}

	public static JsonNode sendGet(VendorApi<?> vendor, String requestUrl, Map<String, String> urlParams,
			Map<String, String> headers) {
		return send(vendor, "GET", requestUrl, null, urlParams, headers);
	}

	private static String urlEncode(String value) {
		try {
			value = URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return value;
	}

	private static JsonNode send(VendorApi<?> vendor, String method, String requestUrl, String payload,
			Map<String, String> urlParams, Map<String, String> headers) {
		JsonNode result = null;
		StringBuilder responseBody = new StringBuilder();
		if (DEBUG) {
			System.out.println(method + " requestUrl=" + requestUrl);
		}
		try {
			StringBuilder uri = new StringBuilder(requestUrl);
			if (urlParams != null && urlParams.size() > 0) {
				uri.append("?")
						.append(urlParams.entrySet().stream()
								.map(entry -> new StringBuilder(entry.getKey()).append("=")
										.append(urlEncode(entry.getValue())).toString())
								.collect(Collectors.joining("&")));

			}
			URL url = new URL(uri.toString());
			HttpsURLConnection httpConector = (HttpsURLConnection) url.openConnection();
			httpConector.setConnectTimeout(TIMEOUT);
			httpConector.setRequestMethod(method);

			httpConector.setRequestProperty("Content-Type", "application/json");
			httpConector.setRequestProperty("Accept", "application/json");
			if (headers != null && !headers.isEmpty()) {
				headers.entrySet().forEach(key -> httpConector.addRequestProperty(key.getKey(), key.getValue()));
			}
			if (DEBUG) {
				for (String k : httpConector.getRequestProperties().keySet()) {
					System.out.println("k=[" + k + "];val=" + httpConector.getRequestProperties().get(k));
				}
				System.out.println("requestUrl=" + requestUrl);
			}
			if (payload != null) {
				httpConector.setDoOutput(true);
				DataOutputStream output = new DataOutputStream(httpConector.getOutputStream());
				output.writeBytes(payload);
				if (DEBUG) {
					System.out.println("request payload=" + payload);
				}
				output.flush();
				output.close();
			}

			if (DEBUG) {
				System.out.println("Response Code : " + httpConector);
			}

			InputStream responseStream;
			if (httpConector.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
				responseStream = httpConector.getInputStream();
			} else {
				responseStream = httpConector.getErrorStream();
			}
			responseBody.append(
					new BufferedReader(new InputStreamReader(responseStream)).lines().collect(Collectors.joining()));
			if (DEBUG) {
				System.out.println("response body=" + responseBody);
			}
			result = new ObjectMapper().readTree(responseBody.toString());
		} catch (SocketTimeoutException exception) {
			responseBody.append(vendor.getErrorHandler().writeErrorJson(ErrorResponse.ERR_TIMEOUT));
			System.err.println(responseBody);
		} catch (IOException ex) {
			ex.printStackTrace();
			responseBody.append(vendor.getErrorHandler().writeErrorJson(ErrorResponse.ERR_UNKNOWN));
		}

		if (DEBUG) {
			System.out.println("requestUrl=" + requestUrl + "; result=" + result);
		}

		return result;
	}

	public static String sendRaw(VendorApi<?> vendor, String method, String requestUrl, String payload,
			Map<String, String> urlParams, Map<String, String> headers) {
		String result = null;
		StringBuilder responseBody = new StringBuilder();

		try {
			StringBuilder uri = new StringBuilder(requestUrl);
			if (urlParams != null && urlParams.size() > 0) {
				uri.append("?")
						.append(urlParams.entrySet().stream()
								.map(entry -> new StringBuilder(entry.getKey()).append("=")
										.append(urlEncode(entry.getValue())).toString())
								.collect(Collectors.joining("&")));

			}
			URL url = new URL(uri.toString());
			HttpsURLConnection httpConector = (HttpsURLConnection) url.openConnection();
			httpConector.setConnectTimeout(TIMEOUT);
			httpConector.setRequestMethod(method);

			httpConector.setRequestProperty("Content-Type", "application/json");
			httpConector.setRequestProperty("Accept", "application/json");
			if (headers != null && !headers.isEmpty()) {
				headers.entrySet().forEach(key -> httpConector.addRequestProperty(key.getKey(), key.getValue()));
			}
			if (DEBUG) {
				for (String k : httpConector.getRequestProperties().keySet()) {
					System.out.println("k=[" + k + "];val=" + httpConector.getRequestProperties().get(k));
				}
				System.out.println("requestUrl=" + requestUrl);
			}
			if (payload != null) {
				httpConector.setDoOutput(true);
				DataOutputStream output = new DataOutputStream(httpConector.getOutputStream());
				output.writeBytes(payload);
				if (DEBUG) {
					System.out.println("request payload=" + payload);
				}
				output.flush();
				output.close();
			}

			if (DEBUG) {
				System.out.println("Response Code : " + httpConector);
			}

			InputStream responseStream;
			if (httpConector.getResponseCode() < HttpURLConnection.HTTP_BAD_REQUEST) {
				responseStream = httpConector.getInputStream();
			} else {
				responseStream = httpConector.getErrorStream();
			}
			responseBody.append(
					new BufferedReader(new InputStreamReader(responseStream)).lines().collect(Collectors.joining()));
			if (DEBUG) {
				System.out.println("response body=" + responseBody);
			}
			result = responseBody.toString();
		} catch (SocketTimeoutException exception) {
			responseBody.append(vendor.getErrorHandler().writeErrorJson(ErrorResponse.ERR_TIMEOUT));
			System.err.println(responseBody);
		} catch (IOException ex) {
			ex.printStackTrace();
			responseBody.append(vendor.getErrorHandler().writeErrorJson(ErrorResponse.ERR_UNKNOWN));
		}

		return result;
	}
}
