package dd.iot.transport;

import java.util.UUID;

import dd.iot.vendor.VendorApi;

/**
 * A session for accessing IoT devices.
 * 
 * @author Darcy Danyluk
 *
 */
public final class Session {

	private static final boolean DEBUG = false; // TODO look into better non-logging frameworks for this

	private VendorApi<?> auth;
	private String token, statusMessage;
	private UUID sessionId;

	protected Session(VendorApi<?> auth, UUID sessionId) {
		this.auth = auth;
		this.sessionId = sessionId;
	}

	public String getToken() {
		return this.token;
	}

	public String refreshToken() {
		this.token = auth.authenticate(this);
		if (DEBUG) {
			System.out.println("token=" + this.token);
		}

		return this.token;
	}

	/**
	 * @return the sessionId
	 */
	public UUID getSessionId() {
		return sessionId;
	}

	/**
	 * @return the statusMessage
	 */
	public String getStatusMessage() {
		return statusMessage;
	}

	/**
	 * @param statusMessage
	 *            the statusMessage to set
	 */
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

}
