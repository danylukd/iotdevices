package dd.iot.transport;

import java.util.concurrent.Future;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.util.concurrent.SettableFuture;

public interface ErrorHandler {

	public enum ErrorResponse {
		ERR_NONE, ERR_TOKEN_EXPIRED, ERR_JSON_FORMAT, ERR_TIMEOUT, ERR_BAD_AUTH, ERR_UNKNOWN;

		public boolean isOk() {
			return this == ERR_NONE;
		}
	}

	public ErrorResponse getErrorResponse(Object reference);

	public ErrorResponse isErrors(JsonNode deviceResponse);

	public String writeErrorJson(ErrorResponse errorResponse);

	public static Future<ErrorResponse> createFuture(ErrorResponse errorResopnse) {
		SettableFuture<ErrorResponse> future = SettableFuture.create();
		future.set(errorResopnse);
		return future;
	}
}
