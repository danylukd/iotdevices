package dd.iot.transport;

public interface AuthAccessor {

	public enum CredentialType {
		USERNAME, PASSWORD, API_KEY, PIN;
	}

	public void setText(String text);

}
